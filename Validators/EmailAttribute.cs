﻿using Authenticator.Data;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;


namespace Authenticator.Validators
{
    public class EmailAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            UserManager<AuthUser> manager = (UserManager<AuthUser>)validationContext.GetService(typeof(UserManager<AuthUser>));

            if (value.ToString() != "")
            {
                Task<AuthUser> task = manager.FindByEmailAsync(value.ToString());
                AuthUser user = task.Result;

                if (user != null)
                {
                    return new ValidationResult(new string("Email is already assigned."));
                }
                return ValidationResult.Success;

            }
            return new ValidationResult("The Email field is required.");

        }
    }
}
