﻿using Authenticator.Data;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Authenticator.Validators
{
    public class UserNameAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            UserManager<AuthUser> manager = (UserManager<AuthUser>)validationContext.GetService(typeof(UserManager<AuthUser>));
            if (value.ToString() != "")
            {
                Task<AuthUser> task = manager.FindByNameAsync(value.ToString());
                AuthUser user = task.Result;

                string pattern = "^[a-zA-Z][a-zA-Z0-9]{3,9}$";
                Regex regex = new Regex(pattern);


                if (user != null)
                {
                    return new ValidationResult(new string("Username taken."));
                }
                else if (!regex.IsMatch(value.ToString()))
                {
                    return new ValidationResult(new string("Incorrect username."));
                }
                return ValidationResult.Success;

            }
            return new ValidationResult("The Username field is required.");

        }
    }
}
