﻿using Authenticator.Validators;
using System.ComponentModel.DataAnnotations;

namespace Authenticator.Models
{
    public class RegisterVM
    {
        [UserName]
        public string Username { get; set; }

        [EmailAddress]
        [Email]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [Required, DataType(DataType.Password), StringLength(20, ErrorMessage = "The password is to short.", MinimumLength = 6)]
        public string Password { get; set; }

        [Required, Compare(nameof(Password), ErrorMessage = "The password and confirmation password do not match.")]
        public string Confirmpassword { get; set; }
    }
}
