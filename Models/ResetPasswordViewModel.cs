﻿namespace Authenticator.Models
{
    public class ResetPasswordVM
    {
        public string Email { get; set; }
    }
}
