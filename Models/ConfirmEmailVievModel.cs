﻿namespace Authenticator.Models
{
    public class ConfirmEmailVM
    {
        public string Code { get; set; }
        public string UserId { get; set; }
    }
}
