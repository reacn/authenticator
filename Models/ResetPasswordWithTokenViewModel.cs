﻿namespace Authenticator.Models
{
    public class ResetPasswordWithTokenVM
    {
        public string UserId { get; set; }
        public string Code { get; set; }
        public string Newpassword { get; set; }
        public string Repeatnewpassword { get; set; }
    }
}
