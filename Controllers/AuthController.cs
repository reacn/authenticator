﻿using Authenticator.Data;
using Authenticator.Models;
using Authenticator.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Authenticator.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        readonly UserManager<AuthUser> userManager;
        readonly IEmailSender emailSender;

        public AuthController(UserManager<AuthUser> userManager, IEmailSender emailSender)
        {
            this.userManager = userManager;
            this.emailSender = emailSender;
        }

        // GET: api/auth/login?name=xxx&pwd=xxx
        [HttpGet]
        [AllowAnonymous]
        [Route("login")]
        public async Task<ActionResult> GetAsync(string name, string pwd)
        {
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(pwd))
            {
                var user = await userManager.FindByNameAsync(name);
                if (user != null && await userManager.CheckPasswordAsync(user, pwd))
                {
                    if (await userManager.IsEmailConfirmedAsync(user))
                    {
                        var claims = new List<Claim>
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };
                        var roles = await userManager.GetRolesAsync(user);
                        AddRolesToClaims(claims, roles, user.Email);
                        var signigKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Environment.GetEnvironmentVariable("SecretKey")));
                        var token = new JwtSecurityToken(
                        issuer: "https://reacn.pl/",
                        audience: "https://reacn.pl/",
                        expires: DateTime.UtcNow.AddHours(3),
                        claims: claims,
                        signingCredentials: new SigningCredentials(signigKey, SecurityAlgorithms.HmacSha256));

                        return Ok(new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            created = DateTime.UtcNow.ToString("MM/dd/yyyy H:mm"),
                            expiration = token.ValidTo.ToString("MM/dd/yyyy H:mm")
                        });
                    }
                    return Conflict(new string("Email not confirmed"));
                }
                return NotFound(new string("Wrong password or username"));
            }
            return NotFound(new string("Username and password can not be empty"));
        }

        private void AddRolesToClaims(List<Claim> claims, IEnumerable<string> roles, string email)
        {
            foreach (var role in roles)
            {
                var roleClaim = new Claim(ClaimTypes.Role, role);
                claims.Add(roleClaim);               
            }
            var emailClaim = new Claim(ClaimTypes.Email, email);
            claims.Add(emailClaim);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("register")]
        public async Task<ActionResult> Register(RegisterVM rvm)
        {
            var resp0 = await userManager.FindByEmailAsync(rvm.Email);
            var resp1 = await userManager.FindByNameAsync(rvm.Username);

            _ = TryValidateModel(rvm);
            if (resp0 != null || resp1 != null)
            {
                return Conflict();
            }
            else if (ModelState.IsValid)
            {
                AuthUser user = new AuthUser
                {
                    UserName = rvm.Username,
                    Email = rvm.Email
                };
                var result = await userManager.CreateAsync(user, rvm.Password);

                if (result.Succeeded)
                {
                    var code = await userManager.GenerateEmailConfirmationTokenAsync(user);

                    byte[] tokenGeneratedBytes = Encoding.UTF8.GetBytes(code);
                    var codeEncoded = WebEncoders.Base64UrlEncode(tokenGeneratedBytes);

                    string callbackUrl = $"https://reacn.pl/account/confirmemail/{user.Id}/{codeEncoded}";

                    await emailSender.ConfirmEmailAsync(rvm.Email, rvm.Username, callbackUrl);

                    return Ok(new string("The verification email has been already sent"));
                }
                return BadRequest();
            }
            return BadRequest();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("confirmemail")]
        public async Task<IActionResult> ConfirmEmail(ConfirmEmailVM cevm)
        {
            if (cevm.UserId == null || cevm.Code == null)
            {
                return NotFound(new string("Empty request body"));
            }

            var fuser = await userManager.FindByIdAsync(cevm.UserId);
            if (fuser != null)
            {
                var codeDecodedBytes = WebEncoders.Base64UrlDecode(cevm.Code);
                var codeDecoded = Encoding.UTF8.GetString(codeDecodedBytes);

                var result = await userManager.ConfirmEmailAsync(fuser, codeDecoded);

                if (result.Succeeded)
                {
                    return Ok(new string("Email confirmed"));
                }

                return Ok(new string("Email not confirmed "));
            }
            return BadRequest();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("resetpassword")]
        public async Task<ActionResult> ResetPassword(ResetPasswordVM rpvm)
        {
            var user = await userManager.FindByEmailAsync(rpvm.Email);

            if (user != null)
            {
                var code = await userManager.GeneratePasswordResetTokenAsync(user);

                byte[] tokenGeneratedBytes = Encoding.UTF8.GetBytes(code);
                var codeEncoded = WebEncoders.Base64UrlEncode(tokenGeneratedBytes);

                string callbackUrl = $"https://reacn.pl/account/resetpassword/{user.Id}/{codeEncoded}";

                await emailSender.ResetPasswordAsync(rpvm.Email, callbackUrl);

                return Ok("The email with link to change password has been already sent");
            }

            return NotFound("No registered user at the provided email address" + rpvm.Email);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("resetpasswordwithtoken")]
        public async Task<ActionResult> ResetPasswordwithtoken(ResetPasswordWithTokenVM rpwtvm)
        {
            if (rpwtvm.UserId == null || rpwtvm.Code == null || rpwtvm.Newpassword == null || rpwtvm.Repeatnewpassword == null)
            {
                return NotFound(new string("Body can't be empty"));
            }

            var fuser = await userManager.FindByIdAsync(rpwtvm.UserId);
            if (fuser != null)
            {
                var codeDecodedBytes = WebEncoders.Base64UrlDecode(rpwtvm.Code);
                var codeDecoded = Encoding.UTF8.GetString(codeDecodedBytes);

                var result = await userManager.ResetPasswordAsync(fuser, codeDecoded, rpwtvm.Newpassword);

                if (result.Succeeded)
                {
                    return Ok(new string("Password changed"));
                }

                return Ok(new string("Password not changed"));
            }
            return NotFound(new string("Whoops! User not found"));
        }

        [HttpPost]
        [Route("deleteaccount")]
        public async Task<ActionResult> Deleteacount(AccountDeleteConfirmation adc)
        {
            var handler = new JwtSecurityTokenHandler();
            string authHeader = Request.Headers["Authorization"];
            authHeader = authHeader.Replace("Bearer ", "");
            var token = handler.ReadToken(authHeader) as JwtSecurityToken;
            var userid = token.Subject;

            var user = await userManager.FindByIdAsync(userid);
            if (user != null && adc.confirm == true)
            {
                var rolesForUser = await userManager.GetRolesAsync(user);

                if (rolesForUser.Count() > 0)
                {
                    foreach (var item in rolesForUser.ToList())
                    {
                        var result = await userManager.RemoveFromRoleAsync(user, item);
                    }
                }

                await userManager.DeleteAsync(user);
                return Ok("Account deleted");
            }

            return NotFound(new string("Woops! Something went wrong"));
        }

        [AllowAnonymous]
        [HttpPut("email")]
        [Route("resendemail")]
        public async Task<ActionResult> Resendconfirmationemail(string email)
        {
            var fuser = await userManager.FindByIdAsync(email);
            if (fuser != null)
            {
                if(await userManager.IsEmailConfirmedAsync(fuser))
                {
                    return Ok();
                }

            }
            return NotFound();
        }
    }
}