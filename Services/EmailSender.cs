﻿using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using MimeKit;
using MimeKit.Utils;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Authenticator.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly IHostingEnvironment _env;

        public EmailSender(IHostingEnvironment env)
        {
            _env = env;
        }

        public async Task ConfirmEmailAsync(string email, string username, string callbackUrl)
        {
            try
            {
                var pathToFile = _env.WebRootPath
                           + Path.DirectorySeparatorChar.ToString()
                           + "Temps"
                           + Path.DirectorySeparatorChar.ToString()
                           + "EmailTemp0.html";
                var pathToLogo = _env.WebRootPath
                    + Path.DirectorySeparatorChar.ToString()
                    + "Temps"
                    + Path.DirectorySeparatorChar.ToString()
                    + "Images"
                    + Path.DirectorySeparatorChar.ToString()
                    + "Logo.png";

                var builder = new BodyBuilder();
                using (StreamReader SourceReader = System.IO.File.OpenText(pathToFile))
                {
                    var image = builder.Attachments.Add(pathToLogo);
                    image.ContentId = MimeUtils.GenerateMessageId();
                    builder.HtmlBody = SourceReader.ReadToEnd()
                        .Replace("{0}", image.ContentId)
                        .Replace("{1}", username)
                        .Replace("{2}", callbackUrl);
                }

                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("", Environment.GetEnvironmentVariable("Email")));
                message.To.Add(new MailboxAddress("", email));
                message.Subject = @"Confirm your email";
                message.Body = builder.ToMessageBody();
                var client = new SmtpClient();
                await client.ConnectAsync("smtp.gmail.com", 587, false);
                await client.AuthenticateAsync(Environment.GetEnvironmentVariable("Email"), Environment.GetEnvironmentVariable("EmailPassword"));
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }

        public async Task ResetPasswordAsync(string email, string callbackUrl)
        {
            try
            {
                var pathToFile = _env.WebRootPath
                           + Path.DirectorySeparatorChar.ToString()
                           + "Temps"
                           + Path.DirectorySeparatorChar.ToString()
                           + "EmailTemp1.html";
                var pathToLogo = _env.WebRootPath
                    + Path.DirectorySeparatorChar.ToString()
                    + "Temps"
                    + Path.DirectorySeparatorChar.ToString()
                    + "Images"
                    + Path.DirectorySeparatorChar.ToString()
                    + "Logo.png";

                var builder = new BodyBuilder();
                using (StreamReader SourceReader = System.IO.File.OpenText(pathToFile))
                {
                    var image = builder.Attachments.Add(pathToLogo);
                    image.ContentId = MimeUtils.GenerateMessageId();
                    builder.HtmlBody = SourceReader.ReadToEnd()
                        .Replace("{0}", image.ContentId)
                        .Replace("{1}", email)
                        .Replace("{2}", callbackUrl);
                }

                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("", Environment.GetEnvironmentVariable("Email")));
                message.To.Add(new MailboxAddress("", email));
                message.Subject = @"Change your password";
                message.Body = builder.ToMessageBody();
                var client = new SmtpClient();
                await client.ConnectAsync("smtp.gmail.com", 587, false);
                await client.AuthenticateAsync(Environment.GetEnvironmentVariable("Email"), Environment.GetEnvironmentVariable("EmailPassword"));
                await client.SendAsync(message);
                await client.DisconnectAsync(true);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }
    }
}