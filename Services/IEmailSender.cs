﻿using System.Threading.Tasks;

namespace Authenticator.Services
{
    public interface IEmailSender
    {
        Task ConfirmEmailAsync(string email, string username, string callbackUrl);

        Task ResetPasswordAsync(string email, string callbackUrl);
    }
}
