﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Authenticator.Data
{
    public class AuthDBSeed
    {
        public static async Task InitAsync(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<AuthDBContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<AuthUser>>();
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            context.Database.EnsureCreated();

            if (!context.Users.Any())
            {
                AuthUser user = new AuthUser()
                {
                    Email = "yarakamiy@gmail.com",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = "Admin"
                };
                await userManager.CreateAsync(user, "1qaz@WSX");
                var roleCheck = await RoleManager.RoleExistsAsync("Admin");
                if (!roleCheck)
                {
                    await RoleManager.CreateAsync(new IdentityRole("Admin"));
                    var userCheck = await userManager.IsInRoleAsync(user, "Admin");
                    if (!userCheck)
                    {
                        await userManager.AddToRoleAsync(user, "Admin");
                    }
                }
            }
        }
    }
}
